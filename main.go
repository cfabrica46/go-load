package main

import (
	"fmt"
	"time"
)

func doJobs(jobs []interface{}) func() interface{} {
	var indx int

	ticker := time.NewTicker(time.Second * 1)
	finish := make(chan bool)

	go func() {
		// var aux bool
		for {
			select {
			case <-ticker.C:
				percentOf := indx * 100 / (len(jobs))
				fmt.Printf(" ~~~Recorridos %d/%d(%d%%)~~~ \n", indx, len(jobs), percentOf)
			case <-finish:
				ticker.Stop()
				return
			}
		}
	}()

	return func() interface{} {
		fmt.Printf("Start job N°%d\n", indx+1)
		time.Sleep(time.Second * 2)
		fmt.Printf("Has been completed job N°%d\n", indx+1)

		if len(jobs)-1 == indx {
			fmt.Printf(" ~~~Recorrido completado! %d/%d(%d%%)~~~ \n", len(jobs), len(jobs), 100)
			finish <- true
		}

		defer func() {
			indx++
		}()
		return jobs[indx]
	}
}

func main() {
	jobs := []interface{}{}

	for i := 1; i < 11; i++ {
		jobs = append(jobs, i)
	}

	job := doJobs(jobs)
	for _ = range jobs {
		aux := job()
		fmt.Println(aux)
	}
}
