package main

import (
	"fmt"
	"time"
)

var jobs []job
var idCount int

type job struct {
	ID        int
	Delay     time.Duration
	completed bool
}

func newJob(delay time.Duration) job {
	idCount++
	return job{ID: idCount, Delay: delay}
}

func (j job) Do() {
	fmt.Printf("Start job ID:%d\n", j.ID)
	time.Sleep(j.Delay)
	fmt.Printf("Has been completed job ID:%d\n", j.ID)
}

func getLastCompletedIndex() int {
	for i := 0; i < len(jobs)-1; i++ {
		if jobs[i].completed && !jobs[i+1].completed {
			return i
		}
	}
	return 0
}

func main() {

	finish := make(chan bool)

	for i := 0; i < 10; i++ {
		jobs = append(jobs, newJob(time.Second*2))
	}

	ticker := time.NewTicker(time.Second * 3)

	go func() {
		for {
			select {
			case <-ticker.C:
				index := getLastCompletedIndex()
				percentOf := (index + 1) * 100 / (len(jobs))
				fmt.Printf(" ~~~Recorridos %d/%d(%d%%)~~~ \n", index+1, len(jobs), percentOf)
			case <-finish:
				percentOf := len(jobs) * 100 / (len(jobs))
				fmt.Printf(" ~~~Recorrido completado! %d/%d(%d%%)~~~ \n", len(jobs), len(jobs), percentOf)
				return
			}
		}
	}()

	go func() {
		for i := range jobs {
			jobs[i].Do()
			jobs[i].completed = true
		}
		finish <- true
	}()

	fmt.Scanln()
}
